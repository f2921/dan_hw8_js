let openList = document.querySelector('#optionsList');
let parentOpenList = openList.parentElement;
let childrenOpenList = openList.childNodes;

console.log(openList);
console.log(parentOpenList);
console.log(childrenOpenList);

let testParagraph = document.querySelector('#testParagraph').innerHTML = "Это параграф";

let mainHeader = document.querySelector('.main-header');
mainHeader.classList.add("nav-item")

console.log(mainHeader);

let sectionTitle = document.querySelectorAll('.section-title');

sectionTitle.forEach(function (el){
    el.remove('section-title');
});

let colorChange = document.getElementsByTagName('p');
for(let pp of colorChange){
    pp.style.backgroundColor = "#ff0000";
}


// 1.Опишите своими словами что такое Document Object Model (DOM)
// Объектная Модель Документа (DOM) является программным интерфейсом для HTML, XML и SVG документов.
//     Это обеспечивает структурированное представление документа (дерева), и определяет способ,
//     по которому структура может быть доступна для программы, для изменения структуры документа,
//     его стиля и содержания. DOM обеспечивает представление документа в виде структурированной
// группы узлов и объектов, которые имеют свойства и методы. По сути, она связывает веб -страницы
// со скриптами или языками программирования.
//
//     2.    Какая разница между свойствами HTML-элементов innerHTML и innerText?
//         innerText возвращает весь текст содержащийся в элементе и в его дочерних елементах.
//     innerHtml возвращает весь текст, включая html теги, которые содержаться у элемента.
//
//     3.    Как можно обратиться к элементу страницы с помощью JS? Какой способ лучше?
//     Условно можно сказать что обращаться к элементам в DOM можно двумя различными способами:
//     Использовать последовательное перемещение по объектной структуре до нахождения необходимого элемента.
//     Использовать прямое обращение к элементу по его идентификатору или имени тэга.
//     Второй способ безусловно проще и удобнее и в повседневной практике всегда используют именно его.
//
//     Вы можете напрямую обратится к body используя следующий код document.body.
//     Обращаемся ко второму потомку body (элемент p задан в коде после h1). Код на данном этапе будет иметь вид:
//     document.body.childNodes[1].
//     Обращаемся к текстовому узлу который является первым потомком p и узнаем значение его свойства.
//     Код на данном этапе будет иметь вид: document.body.childNodes[1].childNodes[0].nodeValue
//     С помощью метода getElementById Вы можете напрямую обращаться к элементам по их идентификатору (атрибут id),
// а с помощью свойства innerHTML можно быстро считывать их текстовое содержимое.